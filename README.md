[![pipeline status](https://gitlab.com/guillaume.fenollar/kubectl-health/badges/master/pipeline.svg)](https://gitlab.com/guillaume.fenollar/kubectl-health/commits/master)

# kubectl-health

kubectl-health is a kubectl plugin written in Go. It's goal is to report any malfunction in a cluster by printing errors and warning on standard output.



## Installation

### Download last release
If you don't have go knowledge or don't have go dev environment, best is to download last build's artifact.
Gitlab builds are Linux amd64
```
curl -s -L -o /tmp/kubectl-health.zip https://gitlab.com/guillaume.fenollar/kubectl-health/-/jobs/artifacts/master/download?job=build
sudo unzip -d /usr/bin/ /tmp/kubectl-health.zip
```

### Build for your system
```
DST=$GOPATH/gitlab.com/guillaume.fenollar/ 
mkdir -p $DST && cd $DST
git clone git@gitlab.com:guillaume.fenollar/kubectl-health.git
cd kubectl-health && go build . -o /usr/bin/kubectl-health
```

## Usage
As it's installed as kubectl plugin, usage is made easy by the automatic use of kubeconfig file if located in standard .kube/config location. KUBECONFIG variable is handled the same way kubectl does so a merged kubeconfig is built at runtime if detected.
```
$ kubectl health
INFO Using context cluster-qual
INFO Found 4 nodes, gathering data...
WARNING Conditions detected ! Please review :

LEVEL          NAMESPACE  KIND NAME                        MESSAGE
ALERT         namespace1  pod mydeploy1-xxxxxxxxxx-yyyyy   Pod isn't ready with message : containers with unready status: [container1]
ALERT         namespace2  pod mydeploy2-xxxxxxxxxx-yyyyy   Pod isn't ready with message : containers with unready status: [container2]
WARNING       namespace2  pod kafka-0                      Pod kafka-0's data volume is full at 78%
WARNING                   node qual-master-1               Node's memory available is 21%
WARNING                   node qual-master-1               Node's Image FS is full at 75%
```

It's possible to specify an alternate kubeconfig file, or an alternate context 
```
kubectl health -kubeconfig ~/path-to-kubeconfig
kubectl health -context my-context
```

### Kubelet API stats

By default, kubectl health gathers stats from the Kubelet API. It first get all nodes Kubelet endpoints IP and ports from standard Node objets via the API, then connects to them using Kubeconfig authentication means (certificates or tokens). To that matter, every connection to cluster's nodes is insecure because of Kubelet certificate CN which is very unlikely to match Endpoint name. 
Kubelet API scraping is source of plenty of information about pods and nodes conditions, but if you want to deactivate all requests to Kubelet API, you can pass the following parameter :
```
kubectl health -nokubelet
```

## License
MIT License

