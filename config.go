package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	// map for flags
	config            = map[string]flag.Value{}
	defaultKubeconfig = fmt.Sprintf(filepath.Join(os.Getenv("HOME"), ".kube", "config"))
	v                 int
)

func setUpConfig() {
	_ = flag.String("context", "", "(optional) Overrides kubeconfig current context with specified")
	_ = flag.String("kubeconfig", "", "(optional) Absolute path to the kubeconfig file")
	_ = flag.Bool("d", false, "(optional) Enable debug logging to standard output")
	_ = flag.Bool("nokubelet", false, "(optional) Don't requests Kubelets API for stats")

	flag.Parse()
	flag.Visit(func(f *flag.Flag) { config[f.Name] = f.Value })
	setUpLoggingLevel()
}

func setUpLoggingLevel() {
	_, debug := config["d"]
	if !debug {
		log.SetLevel(logrus.InfoLevel)
	}
}

func checkKubeconfigFiles(p []string) []string {
	newPaths := make([]string, len(p))
	curIndex := 0
	for _, path := range p {
		if path == "" {
			continue
		}
		if _, err := os.Stat(path); os.IsNotExist(err) {
			log.Warnf("Warning : File %v doesn't exist, skipping it\n", path)
			continue
		}
		newPaths[curIndex] = path
		curIndex++
	}
	if newPaths[0] == "" {
		log.Fatal("No kubeconfig file to use")
	}
	return newPaths
}

// LoadConfiguration loads config from flag or files. If kubeconfig flag is set, it loads from target file.
// if KUBECONFIG env variable is set, it aggregates files just like kubectl does. If none of these conditions match,
// it will load default config file from the user's HOME directory.
func LoadConfiguration() clientcmd.ClientConfig {
	overrides := &clientcmd.ConfigOverrides{}
	setUpConfig()
	var p []string
	if config["kubeconfig"] != nil {
		p = append(p, fmt.Sprint(config["kubeconfig"]))
	} else if multipleKubeconfig := os.Getenv("KUBECONFIG"); multipleKubeconfig != "" {
		p = strings.Split(multipleKubeconfig, ":")
	} else {
		p = append(p, defaultKubeconfig)
	}
	allPaths := checkKubeconfigFiles(p)
	loadingRules := clientcmd.ClientConfigLoadingRules{
		Precedence: allPaths,
	}
	mergedConfig, err := loadingRules.Load()
	if err != nil {
		log.Fatal(err)
	}
	if contextFlag, ok := config["context"]; ok {
		mergedConfig.CurrentContext = contextFlag.String()
	}
	log.Info("Using context ", mergedConfig.CurrentContext)
	return clientcmd.NewDefaultClientConfig(*mergedConfig, overrides)
}
