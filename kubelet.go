package main

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// NodeSpecs gathers important informations about node
type NodeSpecs struct {
	Name                string
	Memory              uint64
	CPU                 uint64
	Ephemeral           uint64
	KubeletEndpointIP   string
	KubeletEndpointPort uint32
	StatsSummary        Summary // see kubelet-stats.go
}

// List all nodes to gather only External IP, that is going to be used for kubelet stats
func getNodesSpecs(c *kubernetes.Clientset) ([]*NodeSpecs, error) {
	nodesSpecsArray := []*NodeSpecs{}
	allNodes, err := c.CoreV1().Nodes().List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	for _, node := range allNodes.Items {
		nodeMemory, _ := node.Status.Capacity.Memory().AsInt64()
		nodeCPU, _ := node.Status.Capacity.Cpu().AsInt64()
		nodeEphemeral, _ := node.Status.Capacity.StorageEphemeral().AsInt64()
		nodeEndpointPort := node.Status.DaemonEndpoints.KubeletEndpoint.Port
		if nodeEndpointPort == 0 {
			nodeEndpointPort = 10250
		}

		var nodeEndpointIP string
		for _, addr := range node.Status.Addresses {
			if addr.Type == "ExternalIP" {
				nodeEndpointIP = addr.Address
				break
			}
		}
		// 2nd pass if no ExternalIP was found
		if nodeEndpointIP == "" {
			for _, addr := range node.Status.Addresses {
				if addr.Type == "InternalIP" {
					nodeEndpointIP = addr.Address
					break
				}
			}
		}
		nodesSpecsArray = append(nodesSpecsArray, &NodeSpecs{
			Name:                node.Name,
			Memory:              uint64(nodeMemory),
			CPU:                 uint64(nodeCPU),
			Ephemeral:           uint64(nodeEphemeral),
			KubeletEndpointIP:   nodeEndpointIP,
			KubeletEndpointPort: uint32(nodeEndpointPort),
			StatsSummary:        Summary{},
		})
	}
	if len(nodesSpecsArray) == 0 {
		return nil, fmt.Errorf("No valid node detected")
	}
	return nodesSpecsArray, nil
}

func getKubeletClient(r *rest.Config) (h *http.Client, token string, e error) {
	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
	}
	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
	}
	if r.CertFile != "" && r.KeyFile != "" {
		kubeletClientCert, err := ioutil.ReadFile(r.CertFile)
		if err != nil {
			return nil, "", err
		}
		kubeletClientKey, err := ioutil.ReadFile(r.KeyFile)
		if err != nil {
			return nil, "", err
		}
		cert, err := tls.X509KeyPair(kubeletClientCert, kubeletClientKey)

		tlsConfig.Certificates = []tls.Certificate{cert}
	}
	if r.BearerTokenFile != "" {
		tokenBytes, err := ioutil.ReadFile(r.BearerTokenFile)
		if err != nil {
			log.Error("Couldn't read token from file ", r.BearerTokenFile)
		}
		token = string(tokenBytes)
	}
	if r.BearerToken != "" {
		token = r.BearerToken
	}

	return &http.Client{
		Transport: tr,
	}, token, nil
}

type NodeMetricsError struct {
	mess error
}

func (n *NodeMetricsError) Error() string {
	return n.mess.Error()
}

func getNodeMetrics(NodeSpecs *NodeSpecs, client *http.Client, token string) ([]byte, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("https://%s:%d/stats/summary",
		NodeSpecs.KubeletEndpointIP, NodeSpecs.KubeletEndpointPort), nil)
	if token != "" {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", token))
	}
	log.Debugf("Getting metrics from node %v with address %v:%v", NodeSpecs.Name, NodeSpecs.KubeletEndpointIP, NodeSpecs.KubeletEndpointPort)
	resp, err := client.Do(req)
	if err != nil {
		return nil, &NodeMetricsError{mess: err}
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("received HTTP status %s", resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}

func unmarshallJSON(body []byte) (Summary, error) {
	// see kubelet-stats.go for summary definition
	data := Summary{}
	err := json.Unmarshal(body, &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func checkNodeMemory(n *NodeSpecs) {
	availableMemory := *n.StatsSummary.Node.Memory.AvailableBytes
	capacityMemory := n.Memory
	perAvailableMemory := 100 * availableMemory / capacityMemory
	if perAvailableMemory <= nodeMemoryLowPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's memory available is %v%%", perAvailableMemory))
		return
	}
	if perAvailableMemory <= nodeMemoryLowPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's memory available is %v%%", perAvailableMemory))
	}
}

func checkNodeCPU(n *NodeSpecs) {
	// Calculate the amount (%) of available CPU
	perUsedCPU := 100 * (*n.StatsSummary.Node.CPU.UsageNanoCores / 1000000000) / n.CPU
	if perUsedCPU >= nodeCPUUsedPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's CPU usage is %v%%", perUsedCPU))
		return
	}
	if perUsedCPU >= nodeCPUUsedPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's CPU usage is %v%%", perUsedCPU))
	}
}

func checkRootFS(n *NodeSpecs) {
	perUsedRootFS := 100 - (100 * *n.StatsSummary.Node.Fs.AvailableBytes / *n.StatsSummary.Node.Fs.CapacityBytes)
	if perUsedRootFS >= nodeRootFSUsedPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's Root FS is full at %v%%", perUsedRootFS))
		return
	}
	if perUsedRootFS >= nodeRootFSUsedPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's Root FS is full at %v%%", perUsedRootFS))
	}
}
func checkRootFSInodes(n *NodeSpecs) {
	if *n.StatsSummary.Node.Fs.Inodes == 0 {
		return // apply
	}
	perUsedRootFSInodes := 100 - (100 * *n.StatsSummary.Node.Fs.InodesFree / *n.StatsSummary.Node.Fs.Inodes)
	if perUsedRootFSInodes >= nodeRootFSUsedPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's Root FS inodes are full at %v%%", perUsedRootFSInodes))
		return
	}
	if perUsedRootFSInodes >= nodeRootFSUsedPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's Root FS inodes are full at %v%%", perUsedRootFSInodes))
	}
}

func checkImageFS(n *NodeSpecs) {
	perUsedImageFS := 100 - (100 * *n.StatsSummary.Node.Runtime.ImageFs.AvailableBytes / *n.StatsSummary.Node.Runtime.ImageFs.CapacityBytes)
	if perUsedImageFS >= nodeRootFSUsedPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's Image FS is full at %v%%", perUsedImageFS))
		return
	}
	if perUsedImageFS >= nodeRootFSUsedPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's Image FS is full at %v%%", perUsedImageFS))
	}
}

func checkImageFSInodes(n *NodeSpecs) {
	if *n.StatsSummary.Node.Runtime.ImageFs.Inodes == 0 {
		return // doesn't apply
	}
	perUsedImageFSInodes := 100 - (100 * *n.StatsSummary.Node.Runtime.ImageFs.InodesFree / *n.StatsSummary.Node.Runtime.ImageFs.Inodes)
	if perUsedImageFSInodes >= nodeRootFSUsedPercentThresholdCritical {
		newReport(ERROR, "node", n.Name, "",
			fmt.Sprintf("Node's CPU Image FS inodes are full at %v%%", perUsedImageFSInodes))
		return
	}
	if perUsedImageFSInodes >= nodeRootFSUsedPercentThresholdWarning {
		newReport(WARNING, "node", n.Name, "",
			fmt.Sprintf("Node's CPU Image FS inodes are full at %v%%", perUsedImageFSInodes))
	}
}

func checkPodsStorage(n *NodeSpecs) {
	for _, pod := range n.StatsSummary.Pods {
		if pod.VolumeStats != nil {
			for _, volume := range pod.VolumeStats {
				if volume.PVCRef != nil {
					// We only care about volumes with PVCRef
					checkPodVolumeSpace(pod.PodRef, &volume)
					checkPodVolumeInodes(pod.PodRef, &volume)
				}
			}
		}
	}
}

func checkPodVolumeSpace(p PodReference, v *VolumeStats) {
	perUsedVolume := 100 - (100 * *v.AvailableBytes / *v.CapacityBytes)
	if perUsedVolume >= podVolumeUsedPercentThresholdCritical {
		newReport(ERROR, "pod", p.Name, p.Namespace,
			fmt.Sprintf("Pod %v's %v volume is full at %v%%", p.Name, v.Name, perUsedVolume))
		return
	}
	if perUsedVolume >= podVolumeUsedPercentThresholdWarning {
		newReport(WARNING, "pod", p.Name, p.Namespace,
			fmt.Sprintf("Pod %v's %v volume is full at %v%%", p.Name, v.Name, perUsedVolume))
	}
}

func checkPodVolumeInodes(p PodReference, v *VolumeStats) {
	if *v.Inodes == 0 {
		return // doesn't apply
	}
	perUsedInodes := 100 - (100 * *v.InodesFree / *v.Inodes)
	if perUsedInodes >= podVolumeUsedPercentThresholdCritical {
		newReport(ERROR, "pod", p.Name, p.Namespace,
			fmt.Sprintf("Pod %v's %v volume inodes are full at %v%%", p.Name, v.Name, perUsedInodes))
		return
	}
	if perUsedInodes >= podVolumeUsedPercentThresholdWarning {
		newReport(WARNING, "pod", p.Name, p.Namespace,
			fmt.Sprintf("Pod %v's %v volume inodes are full at %v%%", p.Name, v.Name, perUsedInodes))
	}

}

func checkKubeletAPI(c *kubernetes.Clientset, restConfig *rest.Config, cKubelet *chan error) {
	// We get nodes specs from the API first
	nodesSpecsArray, err := getNodesSpecs(c)
	if err != nil {
		*cKubelet <- err
		return
	}
	client, token, err := getKubeletClient(restConfig)
	if err != nil {
		*cKubelet <- err
		return
	}
	// We then contact all nodes from their Kubelet endpoint for stats
	for _, nodeSpecs := range nodesSpecsArray {
		nodeMetrics, err := getNodeMetrics(nodeSpecs, client, token)
		if err != nil {
			if _, ok := err.(*NodeMetricsError); ok {
				continue
			} else {
				*cKubelet <- err
				return
			}
		}
		data, err := unmarshallJSON(nodeMetrics)
		if err != nil {
			*cKubelet <- err
			return
		}
		nodeSpecs.StatsSummary = data
		// Individual checks below
		checkNodeMemory(nodeSpecs)
		checkNodeCPU(nodeSpecs)
		checkImageFS(nodeSpecs)
		checkImageFSInodes(nodeSpecs)
		checkRootFS(nodeSpecs)
		checkRootFSInodes(nodeSpecs)
		checkPodsStorage(nodeSpecs)
	}

	*cKubelet <- nil
}
