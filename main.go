/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"os"

	//"k8s.io/apimachinery/pkg/api/errors"
	"github.com/sirupsen/logrus"
	"k8s.io/client-go/kubernetes"
)

var (
	allReports []*report
	// Retain number of triggered checks for usage in select
	launchedChecks int
	// ERROR loglevel for reports, any event that can't be ignored
	ERROR = loglevel{1, "ALERT"}
	// WARNING loglevel for reports that can be hidden by default
	WARNING = loglevel{2, "WARNING"}
	// Default formatter is loglevel namespace kind name message
	reportFormatterColumns = []interface{}{"LEVEL", "NAMESPACE", "KIND", "NAME", "MESSAGE"}
	reportFormatter        string
	log                    = &logrus.Logger{
		Out: os.Stdout,
		Formatter: &logrus.TextFormatter{
			DisableTimestamp:       true,
			DisableLevelTruncation: true,
		},
		Level: logrus.DebugLevel,
	}
)

const (
	ignorePendingPodsForSecondAfterCreation  int = 30  // Don't take accounts newly created (in seconds) pods before pending alert
	nodeMemoryLowPercentThresholdWarning         = 25  // % Available memory on node before warning
	nodeMemoryLowPercentThresholdCritical        = 10  // % Available memory on node before alert
	nodeCPUUsedPercentThresholdWarning           = 75  // % Used CPU on node before warning
	nodeCPUUsedPercentThresholdCritical          = 100 // % Used CPU on node before alert
	nodeRootFSUsedPercentThresholdWarning        = 75  // % Used space on root FS before warning
	nodeRootFSUsedPercentThresholdCritical       = 90  // % Used space on root FS before alert
	nodeImageFSUsedPercentThresholdWarning       = 75  // % Used space on images FS before warning
	nodeImageFSUsedPercentThresholdCritical      = 90  // % Used space on images FS before alert
	podEphemeralUsedPercentThresholdWarning      = 75  // % Used space on pod ephemeral storage before warning
	podEphemeralUsedPercentThresholdCritical     = 90  // % Used space on pod ephemeral storage before warning
	podVolumeUsedPercentThresholdWarning         = 75  // % Used space on pod external volume before warning
	podVolumeUsedPercentThresholdCritical        = 90  // % Used space on pod external volume before warning
)

func main() {

	loadedConfiguration := LoadConfiguration()
	clientConfig, err := loadedConfiguration.ClientConfig()
	clientset, err := kubernetes.NewForConfig(clientConfig)
	if err != nil {
		log.Error(err.Error())
	}

	cNodes := make(chan error)
	cPods := make(chan error)
	cKubelet := make(chan error)

	// Check nodes ready state and conditions
	launchedChecks++
	go checkNodes(clientset, &cNodes)

	// Check pods in non running state
	launchedChecks++
	go checkPods(clientset, &cPods)

	if _, ok := config["nokubelet"]; !ok {
		// Check kubelet API stats for runtime issues
		launchedChecks++
		go checkKubeletAPI(clientset, clientConfig, &cKubelet)
	}

	// Error handling for function channels.
	for i := 0; i < launchedChecks; i++ {
		select {
		case errNodes := <-cNodes:
			if errNodes != nil {
				log.Fatal(errNodes)
			}
		case errPods := <-cPods:
			if errPods != nil {
				log.Fatal(errPods)
			}
		case errKubelet := <-cKubelet:
			if errKubelet != nil {
				log.Fatal(errKubelet)
			}
		}
	}

	printReports()

}
