package main

import (
	"fmt"
	"time"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func checkNodeReady(nodeName string, c v1.NodeCondition) {
	if c.Status != "True" {
		newReport(ERROR, "node", nodeName, "", fmt.Sprint("Status of node is NotReady"))
	}
}

func checkCondition(wantedCondition string, nodeName string, c v1.NodeCondition) {
	var nc string
	// Let's convert NodeCondition to string for comparaison to work
	nc = string(c.Status)
	if nc != wantedCondition {
		newReport(ERROR, "node", nodeName, "", fmt.Sprintf("Condition %v (%v) of node is met", c.Type, nc))
	} else {
		if conditionWasTriggeredRecently(nodeName, c) {
			newReport(WARNING, "node", nodeName, "", fmt.Sprintf("Condition %v was triggered within the last day on node", c.Type))
		}
	}
}

func conditionWasTriggeredRecently(nodeName string, c v1.NodeCondition) bool {
	timeRange := time.Now().Add(time.Hour * -24) // yesterday
	if c.LastTransitionTime.After(timeRange) {
		return true
	}
	return false
}

func checkNodes(c *kubernetes.Clientset, cNodes *chan error) {

	nodes, err := c.CoreV1().Nodes().List(metav1.ListOptions{})
	if err != nil {
		*cNodes <- err
	}
	log.Infof("Found %d nodes, gathering data...\n", len(nodes.Items))
	for _, n := range nodes.Items {
		nodeName := n.Name
		nodeConditions := n.Status.Conditions
		for _, c := range nodeConditions {
			switch c.Type {
			case "Ready":
				checkNodeReady(nodeName, c)
			case "KubeletConfigOk":
				checkCondition("True", nodeName, c)
			default:
				checkCondition("False", nodeName, c)
			}
		}
	}
	*cNodes <- nil

}
