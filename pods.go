package main

import (
	"fmt"
	"time"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func podIsTooRecent(p *v1.Pod) bool {
	podCreationTime := p.ObjectMeta.CreationTimestamp
	timeRange := time.Now().Add(-time.Second * time.Duration(ignorePendingPodsForSecondAfterCreation))
	if timeRange.Before(podCreationTime.Time) {
		// Pod is too recent to be taken into account
		return true
	}
	return false
}

// notRunningPod excludes Job pods
func checkPodConditions(p *v1.Pod) {
	podName := p.Name
	podNamespace := p.ObjectMeta.Namespace
	for _, c := range p.Status.Conditions {
		switch c.Type {
		case "Initialized":
			if c.Status != "True" {
				newReport(ERROR, "pod", podName, podNamespace, fmt.Sprint("Pod is not initialized"))
				return
			}
		case "PodScheduled":
			if c.Status != "True" {
				newReport(ERROR, "pod", podName, podNamespace, fmt.Sprint("Pod is not scheduled"))
				return
			}
		}
	}

	// Containers conditions
	for _, c := range p.Status.ContainerStatuses {
		if !c.Ready {
			var podContainerState string
			if c.State.Waiting != nil {
				podContainerState = c.State.Waiting.Reason
			} else if c.State.Terminated != nil {
				podContainerState = c.State.Terminated.Reason
			}
			if podContainerState != "" {
				newReport(ERROR, "pod", podName, podNamespace,
					fmt.Sprintf("Container %v has status %v", c.Name, podContainerState))
			}
		}

	}
}

func checkPods(c *kubernetes.Clientset, cPods *chan error) {
	pods, err := c.CoreV1().Pods("").List(metav1.ListOptions{
		FieldSelector: "status.phase!=Succeeded",
	})
	if err != nil {
		*cPods <- err
	}
	for _, p := range pods.Items {
		owners := p.ObjectMeta.OwnerReferences
		if len(owners) != 0 {
			if owners[0].Kind == "Job" {
				// Pod is owned by Job, not handled
				continue
			}
		}
		if podIsTooRecent(&p) {
			// Pod was created more than const ignorePendingPodsForSecondAfterCreation ago
			continue
		}
		checkPodConditions(&p)
	}
	*cPods <- nil
}
