package main

import (
	"fmt"
	"sort"
)

type loglevel struct {
	level int
	label string
}
type report struct {
	loglevel
	resKind      string
	resName      string
	resNamespace string // optional
	message      string
}

func (r report) String() string {
	return fmt.Sprintf(reportFormatter, r.loglevel.label, r.resNamespace, r.resKind, r.resName, r.message)
}

func newReport(l loglevel, k, n, ns, m string) {
	log.Debugf("Adding report for %v %v, with message %v", k, n, m)
	allReports = append(allReports, &report{l, k, n, ns, m})
}

func printReports() {
	log.Debugf("End of checks, printing %v reports", len(allReports))
	if allReports != nil {
		// sort by nodes first
		sort.Slice(allReports, func(i, j int) bool {
			return allReports[i].resKind == "node"
		})
		reportFormatter = setFormatter(allReports)
		log.Warnf("Conditions detected ! Please review :\n\n")
		// print Header
		fmt.Printf(reportFormatter, reportFormatterColumns...)
		for _, r := range allReports {
			// print Report
			fmt.Print(r)
		}
	} else {
		log.Info("All good !")
	}
}

func setFormatter(allReports []*report) string {
	// Let's set max ressources name width to pretty print reports
	var additionalGap = 3
	var maxNSWidth = 10
	var maxNameWidth = 6
	for _, r := range allReports {
		if ns := len(r.resNamespace); ns > maxNSWidth {
			maxNSWidth = ns + additionalGap
		}
		if name := len(r.resName); name > maxNameWidth {
			maxNameWidth = name + additionalGap
		}
	}
	return fmt.Sprintf("%%-8v %%%dv %%5v %%-%dv %%v\n", maxNSWidth, maxNameWidth)
}
